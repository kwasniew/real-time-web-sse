var app = require('express')();
var bodyParser = require('body-parser');
var EventEmitter = require('events');

app.use(bodyParser.text())

var chat = new EventEmitter();

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.post('/message', function(req, res) {
  // TODO: notify messages endpoint about incoming message using EventEmitter
  // send OK reponse to the client (without any data)
  chat.emit('message', req.body);
  res.sendStatus(200);
});

app.get('/messages', function(req, res) {
    // TODO: add your server code here
    // this enpoint should be getting notifications from the POST /message endpoint
    // and stream data to all clients connected to this server
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive'
  });

  function listener(message) {
    res.write('id: ' + new Date().getTime() + '\n');
    res.write('data: ' + message + '\n\n');
  }

  chat.on('message', listener);

  // comment this out to get a memory leak
  res.on('close', function() {
      console.log('removing listener');
      chat.removeListener('message', listener);
  });
});


app.listen(3000, function(){
  console.log('listening on *:3000');
});
