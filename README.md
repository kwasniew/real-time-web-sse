# Real Time Web z Server-Sent Events #


### Jakie mamy opcje robienia Real-Time Web? ###

* Polling
* Long Polling
* WebSockets
* Server-Sent Events

### Czym są Server-Sent Events? ###

* API HTML5 w przeglądarce (EventSource)
* push danych z serwera do przeglądarki
* w drugą stronę HTTP POST z formularza HTML lub JS (AJAX)

### Do czego używać Server-Sent Events? ###

* dane z giełdy
* aukcje internetowe
* relacja z meczu
* powiadomienia
* czat (być może nie potrzebujesz WebSocketów)

### Kod po stronie klienta ###

Domyślnie zdarzenie message. Serwer może kontrolować nazwę zdarzenia polem event (patrz Format Danych)

```javascript
var es = new EventSource('/events');
es.addEventListener('message',function(event) {
   document.getElementById("container").innerHTML = event.data;
}, false);
```

### Kod po stronie serwera ###

Nie potrzeba żadnych bibliotek. Wystarczy ustawić nagłówki HTTP i wysyłać dane w odpowiednim formacie

```javascript
res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
		'Connection': 'keep-alive'
});
res.write('data: ' + data + '\n\n');
```

### Format Danych ###

Tylko pole data jest wymagane. Trzeba pamiętać o dodaniu \n na końcu.

* id: 1000\n
* retry: 15000\n
* event: notification\n
* data: {"from": "some person", "text": "hi there"}\n\n

### Obsługa błędów ###

Automatyczny reconnect co 3 sekundy. Serwer może zmienić wartość domyślną w polu retry (patrz Format Danych).

```javascript
es.addEventListener('error', function(event) {
    if (event.target.readyState == EventSource.CLOSED) {
        document.getElementById("container").innerHTML = 'Disconnected';
    } else if (event.target.readyState == EventSource.CONNECTING) {
        document.getElementById("container").innerHTML = 'Connecting...';
    }
}, false)
```

### Wsparcie przeglądarkowe ###

http://caniuse.com/#search=eventsource  
Łatwo napisać polyfill (np. https://github.com/Yaffle/EventSource)  

### Przykład ###

Streaming cytatów w czasie rzeczywistym

* git clone git@github.com:kwasniew/real-time-web.git
* npm i
* node intro.js
* klient: https://github.com/kwasniew/real-time-web/blob/master/intro.html
* server: https://github.com/kwasniew/real-time-web/blob/master/intro.js

### Ćwiczenie ###

Naszym zadaniem będzie zaimplementować idiomatyczny przykład z czatem internetowym.
Zwykle używa się ten przykład do nauki WebSocketów. Chcemy natomiast zobaczyć jak można
ten sam problem rozwiązać z użyciem prostszej technologii jaką jest Server-Sent Events.
Zanim zaczniemy ćwiczenie trener pokaże skończony przykład.

* npm i
* npm start
* dodaj kod klienta w pliku index.html
* dodaj kod serwera w pliku server.js
* przetestuj kod otwierając kilka okien w przeglądarce

Podpowiedź:
Aby wysyłać notyfikacje pomiędzy różnymi częściami kodu możemy użyć wbudowany moduł events.

```javascript
var EventEmitter = require('events');
var emitter = new EventEmitter();
// one part of the code can emit events
emitter.emit('message', 'this is a message');
// some other part of your code can react to events
emitter.on('message', function(data) {
  // your listener code goes here
});
```

### Dodatkowe pytania do ćwiczenia ###

* Czy nasza aplikacja nie jest podatna na wycieki pamięci?
* Co musielibyśmy zmienić w kodzie gdybyśmy musieli wdrożyć tą aplikaję na więcej niż jeden serwer?
* Ustaw pola inne niż wymagane data i zaobserwuj jak Chrome wyświetla te pola w zakładce Networks

### Dlaczego nie WebSockets? ###

* bardziej skomplikowany protokół niż HTTP
* trudniej dopasować do istniejącej infrastruktury
* aby obsłużyć reconnect trzeba dodatkowej biblioteki (np. socket.io - couples front and backend into JS)
* wymyślamy koło na nowo (kompresja, autentykacja, rate limiting, metadane itd.)
* nie można korzystać z optymalizacji w HTTP/2
* https://samsaffron.com/archive/2015/12/29/websockets-caution-required

### SSE gotchas ###

* may prevent service workers from version upgrade
* EventSource does not allow for custom headers (but cookies are added automatically) -> use query params instead